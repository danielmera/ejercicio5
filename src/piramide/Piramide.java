
 
package piramide;

import java.util.Scanner;

/**
 *
 * @author Anderson Mera
 */
public class Piramide {

    Scanner entrada = new Scanner(System.in);
    int Numero;

    public void EntradaDeDato() {
        System.out.println("Digite un numero para su piramide: ");
        Numero = entrada.nextInt();
    }

    public void Proceso() {
        for (int i = 1; i < Numero; i++) {  //filas
            for (int j = Numero - i; j > 0; j--) {   //columnas
                System.out.print(" ");
            }
            for (int k = 0; k < i; k++) {   //columnas
                System.out.print("* ");
            }
            System.out.println("");
        }

    }

    public static void main(String[] args) {
        Piramide entrada = new Piramide();
        entrada.EntradaDeDato();
        entrada.Proceso();
    }
}
